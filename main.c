#include "ecbm_core.h"  //加载库函数的头文件。
#include "stream.h"
#include "stream_modbus.h"
#include "stream_fur.h"
#include "stream_xmodem.h"



u16 log=0;
void es_xmodem_data_in(u8 pack,u8 offset,u8 buf){
    P55=!P55;
    eeprom_write((u16)(pack-1)*128+(u16)offset,buf);
    log++;
}


void main(){            //main函数，必须的。
    system_init();      //系统初始化函数，也是必须的。
    eeprom_init();
    timer_init();       //初始化定时器。
    timer_set_timer_mode(0,2000);//定时器0每2000us中断一次。
    timer_start(0);     //开启定时器0。
    gpio_mode(D55,GPIO_OUT);
    delay_ms(500);
    es_xmodem_start();
    while(1){
        ecbm_stream_main();
    }
}
void tim_fun(void)TIMER0_IT_NUM{
    ECBM_STREAM_TIME_RUN();
}
void ecbm_stream_out(esu8 dat){
    uart_char(1,dat);
}
void uart1_receive_callback(void){
    ECBM_STREAM_IN(SBUF);
}
u8 cmp_on=0,cmp_off=0;
void ecbm_stream_exe(esu8 dat){
    u16 n;
    dat=dat;
    es_fur_exe(dat);
    es_modbus_rtu_exe(dat);
    es_xmodem_exe(dat);
    ecbm_stream_strcmp(dat,"write" ,&cmp_on );
    ecbm_stream_strcmp(dat,"read",&cmp_off);
    if(cmp_on ==5){
        delay_ms(50);
        es_xmodem_start();
    }
    if(cmp_off==4){
        for(n=0;n<4096;n++){
            uart_char(1,eeprom_read(n));
        }
    }
    
}


esu8 es_fur_get_id(void){
    return 1;
}
esu16 es_fur_data_out(esu16 addr){
    addr=addr;
    return 0;
}
void es_fur_data_in(esu16 addr,esu16 dat){
    addr=addr;dat=dat;
}
void es_fur_master_receive_callback(esu16 addr,esu16 dat){
    if(addr==15){
        if(dat==0){
            P55=0;
        }else{
            P55=1;
        }
    }
}




void es_modbus_rtu_crc_err_callback(void){

}
esu8 es_modbus_rtu_get_id(void){
    return 1;
}
void es_modbus_cmd_write_bit(esu16 addr,esu8 dat){
    addr=addr;
    dat=dat;
}
void es_modbus_cmd_read_bit(esu16 addr,esu8 * dat){
    addr=addr;
    *dat=1;
}
void es_modbus_cmd_read_io_reg(esu16 addr,esu16 * dat){
    addr=addr;
    *dat=1;
}
void es_modbus_cmd_read_io_bit(esu16 addr,esu8 * dat){
    addr=addr;
    *dat=1;
}
void es_modbus_cmd_write_reg(esu16 addr,esu16 dat){
    addr=addr;
    dat=dat;
}
void es_modbus_cmd_read_reg(esu16 addr,esu16 * dat){
    addr=addr;
    *dat=1;
}
